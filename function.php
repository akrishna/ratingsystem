<?php 
function returnLastValues($cand1,$cand2,$winner) {
	$lastValues=array();

	$result=MYSQL_QUERY(
		 "SELECT * from record where cand_id='$cand1'"
		);
	if (!$result) {
	    die('Invalid query: ' . mysql_error());
	}
	else {
		if(mysql_num_rows($result)==0) {
			$lastValues['cand1_elo']=100;
			$lastValues['cand1_linear']=100;
			$lastValues['cand1_simple']=100;

			if($winner==$cand1) {

				MYSQL_QUERY(
					"INSERT INTO  record(cand_id,total,win,average_win)".
					"VALUES ('$cand1',1,1,1)"
					);
			}
			else {
				MYSQL_QUERY(
					"INSERT INTO  record(cand_id,total,win,average_win)".
					"VALUES ('$cand1',1,0,0)"
					);
			}
		}
		else {	
			$row=mysql_fetch_array($result);
			$lastValues['cand1_elo']=$row['elo_factor'];
			$lastValues['cand1_linear']=$row['linear_factor'];
			$lastValues['cand1_simple']=$row['simple_factor'];
			if($winner==$cand1) {
				$cdquery="UPDATE record SET total=total+1,win=win+1,average_win=win/total
							WHERE cand_id='$cand1'";
				$cdresult=mysql_query($cdquery) or die ("Query to get data from firsttable failed: ".mysql_error()); 
			}
			else {
			
				$cdquery="UPDATE record SET total=total+1,average_win=win/total 
							WHERE cand_id='$cand1'";
				$cdresult=mysql_query($cdquery) or die ("Query to get data from firsttable failed: ".mysql_error()); 
	
			}

		}



	}

	 
	$result=MYSQL_QUERY(
		 "SELECT * from record where cand_id='$cand2'"
		);
	if (!$result) {
	    die('Invalid query: ' . mysql_error());
	}
	else {
		if(mysql_num_rows($result)==0) {
			$lastValues['cand2_elo']=100;
			$lastValues['cand2_linear']=100;
			$lastValues['cand2_simple']=100;

			if($winner==$cand2) {

				MYSQL_QUERY(
					"INSERT INTO  record(cand_id,total,win)".
					"VALUES ('$cand2',1,1)"
					);
			}
			else {
				MYSQL_QUERY(
					"INSERT INTO  record(cand_id,total,win)".
					"VALUES ('$cand2',1,0)"
					);
			}
		}
		else {
			$row=mysql_fetch_array($result);
			$lastValues['cand2_elo']=$row['elo_factor'];
			$lastValues['cand2_linear']=$row['linear_factor'];
			$lastValues['cand2_simple']=$row['simple_factor'];
			if($winner==$cand2) {
				$cdquery="UPDATE record SET total=total+1,win=win+1,average_win=win/total
							WHERE cand_id='$cand2'";
				$cdresult=mysql_query($cdquery) or die ("Query to get data from firsttable failed: ".mysql_error()); 
			}
			else {
			
				$cdquery="UPDATE record SET total=total+1,average_win=win/total 
							WHERE cand_id='$cand2'";
				$cdresult=mysql_query($cdquery) or die ("Query to get data from firsttable failed: ".mysql_error()); 
	
			}

		}



	}
	return $lastValues;
	 
}
function elo_factor($cand1,$cand2,$winner,$cand1value,$cand2value) {
	$finalvalues=array();
	$cand1_expected=1/(1+pow(10,($cand2value-$cand1value)/100));
	$cand2_expected=1/(1+pow(10,($cand1value-$cand2value)/100));
	if($cand1==$winner) {
		$finalvalues['cand1']=$cand1value+16*(1-$cand1_expected);
		$finalvalues['cand2']=$cand2value+16*(0-$cand2_expected);

	}
	else {
		$finalvalues['cand1']=$cand1value+16*(0-$cand1_expected);
		$finalvalues['cand2']=$cand2value+16*(1-$cand2_expected);

	}
	return $finalvalues;
	
} 

function linear_factor($cand1,$cand2,$winner,$cand1value,$cand2value) {
	$finalvalues=array();
	$cand1_expected=$cand1value/($cand1value+$cand2value);
	$cand2_expected=$cand2value/($cand1value+$cand2value);
	if($cand1==$winner) {
		$finalvalues['cand1']=$cand1value+16*(1-$cand1_expected);
		$finalvalues['cand2']=$cand2value+16*(0-$cand2_expected);

	}
	else {
		$finalvalues['cand1']=$cand1value+16*(0-$cand1_expected);
		$finalvalues['cand2']=$cand2value+16*(1-$cand2_expected);

	}
	return $finalvalues;
	
}

function simple_factor($cand1,$cand2,$winner,$cand1value,$cand2value) {
	$finalvalues=array();
	if($cand1==$winner) {
		if($cand1value>$cand2value) {
			$finalvalues['cand1']=$cand1value+1.6;
			$finalvalues['cand2']=$cand2value-1.6;
		}
		else if($cand1value==$cand2value){
			$finalvalues['cand1']=$cand1value+8;
			$finalvalues['cand2']=$cand2value-8;
			
		}
		else {
			$finalvalues['cand1']=$cand1value+16;
			$finalvalues['cand2']=$cand2value-16;
		}

	}
	else {
		if($cand1value>$cand2value) {
			$finalvalues['cand1']=$cand1value-16;
			$finalvalues['cand2']=$cand2value+16;
		}
		else if($cand1value==$cand2value){
			$finalvalues['cand1']=$cand1value-8;
			$finalvalues['cand2']=$cand2value+8;
			
		}
		else {
			$finalvalues['cand1']=$cand1value-1.6;
			$finalvalues['cand2']=$cand2value+1.6;
		}

	}
	return $finalvalues;
	
}
?>
